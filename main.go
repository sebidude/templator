package main

import (
	"fmt"
	"os"
	"text/template"

	"github.com/spf13/cobra"

	"github.com/Masterminds/sprig/v3"
	"gopkg.in/yaml.v3"
)

var (
	templatefile string
	valuefiles   []string
	output       string

	rootCmd = &cobra.Command{
		Use:   "templator",
		Short: "templator - template all the things",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			renderTemplate()
		},
	}
)

func init() {
	rootCmd.Flags().StringVarP(&templatefile, "template", "t", "", "path to the file containing the template")
	rootCmd.Flags().StringArrayVarP(&valuefiles, "values", "f", []string{""}, "path to the file containing the values")
	rootCmd.Flags().StringVarP(&output, "output", "o", "", "file to write the output to. Defaults to stdout")
}

func readFile(filename string) ([]byte, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func prepareValues() ([]byte, error) {
	var data []byte
	for _, f := range valuefiles {
		content, err := readFile(f)
		if err != nil {
			return nil, err
		}
		data = append(data, content...)
	}
	return data, nil
}

func prepareTarget() (*os.File, error) {
	if len(output) == 0 {
		return os.Stdout, nil
	}

	t, err := os.OpenFile(output, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)
	if err != nil {
		return nil, err
	}
	return t, nil

}

func renderTemplate() error {
	target, err := prepareTarget()
	if err != nil {
		return err
	}
	defer target.Close()

	valuesbytes, err := prepareValues()
	if err != nil {
		return err
	}

	var values interface{}
	err = yaml.Unmarshal(valuesbytes, &values)
	if err != nil {
		return err
	}

	tplbytes, err := readFile(templatefile)
	if err != nil {
		return err
	}

	tpl := template.Must(template.New("base").Funcs(sprig.FuncMap()).Parse(string(tplbytes)))
	if err := tpl.Execute(target, values); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	target.Close()
	return nil

}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Whoops. There was an error while executing your CLI '%s'", err)
		os.Exit(1)
	}
}
