{{- $ca := genCA .caCommonName 3650 }}
ca.crt: {{ $ca.Cert | toString | b64enc }}
ca.key: {{ $ca.Key | toString | b64enc }}


You did it {{ .result }}!