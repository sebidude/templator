package main

import (
	"os"
	"testing"
)

func TestPrepareValues(t *testing.T) {
	t.Run("permission denied on values", func(t *testing.T) {
		_, err := os.OpenFile("noperm", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0200)
		if err != nil {
			t.Error(err)
		}
		valuefiles = []string{"noperm"}
		_, err = prepareValues()
		if !os.IsPermission(err) {
			t.Error("prepare values must fail with permission error")
		}
		defer os.Remove("noperm")

	})

}
